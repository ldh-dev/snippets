#!/bin/bash

yum install MariaDB-server-10.3.12 MariaDB-client-10.3.12 MariaDB-backup-10.3.12 socat

# Run secure install
mysql -e "UPDATE mysql.user SET Password=PASSWORD('ROOT_PASS') WHERE User='root'"
mysql -e "DELETE FROM mysql.user WHERE User=''"
mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
mysql -e "DROP DATABASE IF EXISTS test"
mysql -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
mysql -e "FLUSH PRIVILEGES"

# Create user for mariabackup SST 
mysql -e "CREATE USER 'mariabackup'@'localhost' IDENTIFIED BY 'password123'"
mysql -e "GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'mariabackup'@'localhost'"

